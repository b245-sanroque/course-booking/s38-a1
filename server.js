// Install Packages
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	/*
		- By default, our backend's CORS setting will prevent any application outside our Express JS app to process the request. 
		
		- Using the CORS pacckage, it will allow us to manipulate this and control what applications may use our app.

		- Allows our backend applicaton to be available to our frontend application
		
		- Allows us to control the app's Cross Origin Resource Sharing
	*/
 
// Routes
	const userRoutes = require("./Routes/userRoutes.js");


// Server/app Creation
	const port = 3001;

	// contain express to a variable app
	const app = express();

	//middlewares
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));

	app.use(cors());

//Routing
	//localhost:3001/user/
	app.use("/user", userRoutes);


// establish database connection
	mongoose.set('strictQuery', true);
	//mongoDBconnection
	mongoose.connect("mongodb+srv://admin:admin@batch245-sanroque.9babxkw.mongodb.net/batch245_Course_API_SanRoque?retryWrites=true&w=majority", {
			useNewUrlParser: true,
			useUnifiedTopology: true
		})

	//check database connection
		let db = mongoose.connection
		
		//for error handling
		db.on("error", console.error.bind(console, "Connection Error!"))
		
		//validation of connection
		db.once("open", ()=> console.log("We are now connected to the cloud!!"));


// will console, if server is running successfully
	app.listen(port, () => console.log(`Server is running at port ${port}!!`));